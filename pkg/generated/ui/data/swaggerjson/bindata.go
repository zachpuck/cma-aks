// Code generated by go-bindata.
// sources:
// assets/generated/swagger/api.swagger.json
// DO NOT EDIT!

package swaggerjson

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"github.com/elazarl/go-bindata-assetfs"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

func (fi bindataFileInfo) Name() string {
	return fi.name
}
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}
func (fi bindataFileInfo) IsDir() bool {
	return false
}
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _apiSwaggerJson = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xec\x5a\x5f\x6f\xdb\x38\x12\x7f\xcf\xa7\x18\xe8\x0e\xb8\x1e\x90\xc6\x69\xee\xe5\x90\xa7\x33\x12\xa0\x35\xd2\xdc\x06\x9b\xa2\x7d\x58\x14\xc6\x58\x1a\x4b\xac\x25\x52\x21\x47\x4e\xdd\x45\xbe\xfb\x82\x94\x64\xfd\xb1\x64\x3b\x72\x9b\x1a\x8b\xf6\xa5\x8e\xc9\xf9\xfb\x1b\x0e\x67\x86\xfe\xf3\x04\xc0\x33\x8f\x18\x86\xa4\xbd\x4b\xf0\x2e\xce\xce\xbd\x53\xfb\x9d\x90\x73\xe5\x5d\x82\x5d\x07\xf0\x58\x70\x4c\x76\xfd\x2a\xce\x0c\x93\x86\x5b\x94\x18\x92\x86\xf1\xcd\x3d\xbc\xa3\x38\xb5\x1f\xef\x26\x8e\x14\xc0\x5b\x92\x36\x42\x49\x4b\xb0\x3c\x3f\x7b\x53\xf0\x04\xf0\x7c\x25\x19\x7d\x5e\x33\x06\xf0\x24\x26\x8e\xf3\xad\xf0\x23\xa4\x18\x3e\x92\xa4\x6f\x02\x0b\x0a\x00\x2f\xd3\xb1\x5d\x8f\x98\x53\x73\x39\x1a\x85\x82\x63\x9c\x9d\xf9\x2a\x19\x25\xcb\x7c\xef\xc8\x4f\xf0\x35\x2e\x4c\x45\x43\x09\x0a\x47\x55\x6e\xf9\x5f\x68\xbf\xb1\x54\x9e\xdb\xf3\x74\x02\xf0\xe4\x0c\x35\x7e\x44\x09\x19\xef\x12\xfe\xc8\x75\x74\x82\x4a\x85\xed\x1f\x96\xe2\xb3\xdb\xeb\x2b\x69\xb2\xc6\x66\x4c\xd3\x58\xf8\xc8\x42\xc9\xd1\x17\xa3\x64\xb5\x37\xd5\x2a\xc8\xfc\x3d\xf7\x22\x47\xa6\xf2\xf6\x08\x53\x31\x5a\xbe\x19\xf9\xb9\xb3\xeb\xde\x0a\xa9\xee\x3c\xab\x7e\x96\x24\xa8\x57\xd6\xd6\x4f\x22\x8e\x41\x13\x6b\x41\x4b\x02\x8e\x08\x0c\x23\x67\x06\xd4\x1c\x10\x0a\x66\x80\x32\x00\xc1\x06\x16\xd9\x8c\x7c\x25\xe7\x22\x84\xb9\xd2\xe0\x2b\x29\xc9\x67\xb1\x14\xbc\x5a\xfb\x11\xc0\x53\x29\x69\xa7\xf2\x24\xb0\x32\xde\x12\x17\x21\x50\xdf\xa4\xc9\xa4\x4a\x1a\x32\x0d\xdd\x00\xbc\x8b\xf3\xf3\xd6\x57\x00\x5e\x40\xc6\xd7\x22\xe5\x22\x42\x6a\x8c\x72\x8b\x2c\x20\xb8\x41\x06\xe0\xfd\x53\xd3\xdc\x52\xfc\x63\x14\xd0\x5c\x48\x61\x39\x18\x0b\x3e\x2e\x4c\xa5\xd8\xef\x94\xc6\x2b\xaf\x41\xfb\x74\xd2\xf5\xf9\xa9\x66\x41\x8a\x1a\x13\x62\xd2\x15\x5e\xf9\xbf\x96\xee\x65\xb4\xba\xff\x4f\xb7\xda\xf5\x7f\x4c\xc8\xba\xde\x02\x51\x3a\x9f\x15\xcc\x08\x62\xa5\x16\x14\x40\x96\x9e\xb5\x59\x08\x47\xf9\x90\x91\x5e\xb5\x97\x34\x3d\x64\x42\x93\x45\x61\x8e\xb1\xa1\xd6\x32\xaf\x52\xa7\x98\x61\x2d\x64\x58\x37\xff\xe9\x74\xb7\x39\xbe\xa6\x80\x24\x0b\x8c\xcd\x19\xa6\xe9\x54\x04\x3b\x8c\xfb\x10\x11\x8c\xd3\x74\x12\xb8\xe0\x19\xdf\x4d\x60\xec\xfb\x64\xcc\x51\x5a\xc4\x24\x51\xf2\x1e\x16\x7d\x70\x1b\xd7\x26\xe1\xf1\x9a\x94\xa2\x31\x8f\x4a\xef\x03\xd3\x5d\xb1\xf5\xa7\x98\xb5\xfe\xfc\xb9\x76\xd8\x18\xc3\xf6\x31\x2b\x2f\x96\x8a\xf8\xf3\x49\xcb\x33\x5e\x40\x31\x31\x6d\x4f\x7f\xf9\x9e\x2a\xdd\x6d\x49\x65\xd7\x6e\xeb\x71\x66\xb3\x86\x6e\xc7\x92\xd0\x3e\x45\xc8\x20\x4c\x3d\xa1\xfd\xcb\x80\x25\xb4\x79\x2d\x20\xc3\x5a\xad\x8e\xf2\xb4\xfc\x4a\x69\xbf\x52\xda\x51\xa6\xb4\x34\xdb\x51\xce\xa5\x5a\x2d\x85\xad\xa5\xf7\x4a\x69\x57\x9a\xf0\x58\x53\x5a\x43\xb7\x17\x49\x69\x33\x15\x6c\xc0\x9e\x47\x44\xd7\x4a\x2d\x20\x58\x67\xed\x78\xf8\x0e\x36\xdf\x9a\x70\x1f\x8b\x87\x47\xd5\x49\xcd\x61\xed\x1e\x62\x14\x0b\xc3\xc3\x1a\x09\x04\x4b\x6b\x2b\xd9\x82\x97\xd9\xab\x3f\x78\x6f\x05\x1e\x57\x08\x36\x95\x1b\x14\x83\xdf\x01\x91\xaa\x39\x7e\x16\x18\x99\x96\x50\x90\x82\x6d\xce\x75\xe2\xdc\x0e\x38\x53\x19\x03\xa6\x02\x0c\xe9\xe5\xd6\xf4\xf0\x96\xf8\x63\xce\x61\x52\x31\x38\x3e\x8c\x0a\x1d\x5f\x0c\x9f\xf5\x28\xa0\xa6\x4d\xd5\x8c\x37\xce\xf0\xf8\xe6\xfe\x3e\x25\x7f\x7c\x73\x3f\x91\x86\x51\xfa\xf4\x56\xab\x2c\xad\x63\x59\xde\x1c\x6a\xf6\x85\xfc\xea\x04\x78\xa9\xb6\x68\xb0\x68\x39\xb7\xcc\x56\x0d\x77\xb7\x6e\x9f\xd3\xc6\x5a\x39\x8a\xb1\x17\xa1\xac\xf5\x98\xa1\xd3\xa4\xdb\x2b\x39\xbf\xe7\xcb\x28\xad\x04\xbb\x1b\x5e\xdd\x33\xca\x00\x75\x30\xbd\xbe\x98\x2e\x2f\x4e\x81\xd8\x3f\xfb\x77\xb7\xc8\x44\xc8\xe9\x43\x86\x92\x05\xaf\xfa\x44\x0b\xc9\x14\x36\x22\x16\xc0\xcb\x03\xb3\x58\xfe\xcf\x45\x8f\x62\xb7\x42\x8a\x24\x4b\x40\x66\xc9\x8c\xb4\x75\x81\x28\x54\x35\xf0\x2a\xa0\x39\x66\x31\x1b\x5b\x96\x7e\x23\xad\xfa\x54\xc4\xaf\x3f\x54\x45\xfc\x3a\x4c\xc5\x8d\x2a\x61\x13\x0e\x87\xb5\x01\x17\xb0\x36\x43\x3b\x78\x50\x06\x5d\xc2\xbc\x46\x16\x6a\x9d\xaf\x8e\x7c\x70\x40\x2c\x87\x82\xa7\x9b\xe9\xed\x79\x21\xcd\x18\x82\x92\x79\x44\x0b\x06\x4d\xa9\x32\x82\x95\x5e\x75\x83\x68\x45\xfa\x2a\x49\x04\x0f\x96\x18\xa1\x89\xd6\x87\x48\x30\x14\xec\x7a\xc5\xb1\x26\x9a\x1a\x46\x1e\x76\xa6\x3e\x45\xc4\x91\xc5\x48\x83\x54\xec\xa4\x5a\x8e\xf0\x88\x06\xfc\x98\x50\xc2\x63\x44\x12\x66\x99\x88\x7b\x94\xb0\x4b\xc1\x34\x18\xaa\xc0\x35\xb2\x4b\x1a\x8e\x4d\x8f\x99\xea\x20\x1c\x8b\xa8\xb2\x42\x42\x05\x99\xa1\xc0\xc6\xb9\xaf\x92\x54\xc4\xd4\x2d\xb1\x58\xd4\x83\xe4\x5d\x15\xc4\x4e\x54\x37\xff\x34\x46\xb6\x31\x3e\x88\xff\x5d\x41\x0c\x82\x73\x98\x72\x79\x79\xff\x31\x02\x9d\x49\x29\xa4\x0d\xdb\x8e\x53\xdc\x38\x7d\xf9\x1d\x37\xfe\x96\x69\xba\xaa\x5a\x9f\x43\x8e\x5c\xd1\xda\x0e\x8d\xfd\xae\x86\xb7\xe7\x0e\xc9\x3b\xce\xa1\x82\x3a\xfb\xd0\x1e\xa8\xca\x46\x70\xa8\xac\x9e\xf6\x70\xaf\x14\x6b\xe9\x6b\x5d\xa9\x8d\xdb\xcc\x50\x3e\x22\xb7\x95\x80\xc5\xb9\x36\x00\xf1\x3a\xe0\x2d\x6a\x85\x6b\x62\x14\xf1\x84\x29\x39\x04\xdf\x81\xd8\x4e\xae\x5b\xa3\xe7\x6e\x57\x0f\x2e\x3e\x3a\x86\xdb\xdd\x12\xf2\x27\x88\x81\x89\xb2\x9a\x37\x55\x2f\x19\x3b\x25\x56\x0f\x1b\x07\x4b\xad\xbd\x91\xb8\xfc\xe5\x9e\x48\xec\xc7\x6e\x25\xb6\x1c\xf9\x22\x26\x7e\x45\xc3\x8b\x44\xc3\x36\x20\x3a\xca\xf9\x43\x10\x89\x95\xdf\x2e\x9b\xf6\xb7\xc9\x65\x60\x7b\x19\xc0\x35\x32\xc2\x15\xc9\x5e\xcf\xf9\x9d\xd7\x05\xec\x6a\xa7\x36\xae\x9a\x9e\x0b\xb4\x99\xf1\x5c\x65\xb0\x1b\xcd\xb2\xc4\x9c\xe6\xe5\x68\x9f\x0b\x50\x6b\x6c\x8e\x5b\x3c\xc1\x94\xb4\xf7\xf7\x5a\xb2\x57\x07\x56\x63\xf4\xb4\xa3\x97\x29\xb4\x7d\x7e\xb8\xdc\x9a\xf0\xa7\x74\x7a\xfd\x2f\x89\xeb\xf1\x60\x6f\xd9\x63\x37\x04\x9b\x65\xd5\xde\x53\xab\xbb\x82\x81\x3b\x27\xfd\x51\x5c\xca\x01\x93\x92\x2f\xe6\xc5\x33\xf7\x00\x1f\x37\xe4\xfd\x0c\x67\xd7\xf3\xcc\xda\xaa\xd7\x10\x8b\x05\x01\x2e\x7a\x4a\x96\xc5\x7f\xcd\xc1\x8d\xcf\xb2\x2a\x9a\x6f\xb2\x19\x69\x49\x4c\x3d\xe2\xd0\x9e\xea\xc1\x90\x96\x59\x6f\x4b\x4e\xba\xb9\xef\x05\xb2\xa6\x46\x24\xc2\x68\x8a\x4b\x14\x31\xce\x44\xbc\xa5\x8b\x9e\x29\x65\x1b\x9b\xbe\x2e\xba\x73\x79\x5b\xbf\x54\x1e\x02\x61\xe0\xdd\xb8\xe7\x2e\x23\x7e\x54\x7a\x31\x9d\xe3\x4c\x0b\x7f\x30\x28\x39\x79\x71\xda\x5a\xdd\xc5\xbe\x21\x9d\x0f\xb2\x0e\x88\x65\xb5\x78\x69\xc7\xda\x26\xa7\x96\x5b\x60\xb6\x02\x8e\x84\x01\x4d\x0f\x19\x99\x9e\xb6\x74\xf3\xe7\x2a\xbb\xe3\xb2\x56\x16\xf5\x83\x10\xb8\x4a\xba\x7d\xdf\x97\xca\x40\x39\xb5\xdc\x0f\x9a\x8e\xa7\xd5\xe3\x83\xe6\x4a\x65\xcd\x0b\xd8\x86\x5f\xf1\xc2\xda\x97\xea\x0f\xa8\xad\xee\x1b\xf5\xd4\x26\xc8\x5b\xdc\xd9\x35\x53\x3f\x3e\x7f\x4e\x4c\x33\x6f\xe4\xf3\x25\xb3\x32\x36\xf0\xb6\x05\xf3\x0f\x2b\x6a\x36\xe3\x7f\x77\x05\xf3\xbe\xfd\x0e\xf3\x4c\x84\xfe\x76\xe8\x0c\x48\x35\xb5\xae\x7c\x5f\xef\x35\xde\x23\x8e\xd0\x7b\x73\x58\xbf\xf3\xb8\xc4\xfd\xdb\x4d\xb7\xd3\x8a\x02\x63\x2a\x3a\x87\xbd\x5b\x1c\xb8\x7b\x66\xbc\x7d\x0c\x58\xdf\xb9\xe9\xf4\xcd\x19\x8c\x93\xe2\x46\x2e\x65\x4d\xd4\x48\x49\xd5\x83\x0d\x7d\x65\xd2\x12\xe3\x6b\xe5\xd7\x5e\x6c\x5a\xef\x52\xb7\x4a\x53\xf1\x4a\xb6\xdf\xaf\x57\x9f\xf1\x83\x53\xab\xc9\xc9\xd3\xc9\x5f\x01\x00\x00\xff\xff\x62\xa8\x54\x6c\x46\x2b\x00\x00")

func apiSwaggerJsonBytes() ([]byte, error) {
	return bindataRead(
		_apiSwaggerJson,
		"api.swagger.json",
	)
}

func apiSwaggerJson() (*asset, error) {
	bytes, err := apiSwaggerJsonBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "api.swagger.json", size: 11078, mode: os.FileMode(420), modTime: time.Unix(1536094333, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"api.swagger.json": apiSwaggerJson,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"api.swagger.json": {apiSwaggerJson, map[string]*bintree{}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}

func assetFS() *assetfs.AssetFS {
	assetInfo := func(path string) (os.FileInfo, error) {
		return os.Stat(path)
	}
	for k := range _bintree.Children {
		return &assetfs.AssetFS{Asset: Asset, AssetDir: AssetDir, AssetInfo: assetInfo, Prefix: k}
	}
	panic("unreachable")
}
