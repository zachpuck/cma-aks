package main

import (
	"gitlab.com/mvenezia/cma-aks/cmd/cma-aks/cmd"
)

func main() {
	cmd.Execute()
}
